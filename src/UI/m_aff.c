/*
m_aff.c
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : inmplementation des fonctions defini dans m_aff.h
*/

//#include "m_aff.h"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#include "../system.h"

    #define NB_ENTRY         5
    #define BETWEEN_ENTRY    20


/* ############################ */
/* Fonction d'affichage du menu */
/* ############################ */
void m_aff(t_system *system, int p_cursor)
{
    int i = 0, long_text = 0;
    //Declaration des varialbes d'affichage
    SDL_Rect position;
    SDL_Surface* title;
    SDL_Surface* cursor;
    void *entry[NB_ENTRY];
    TTF_Font *police;
    //Initialisation des variables d'affichage
    cursor          = IMG_Load("./pictures/cursor.bmp");
    police          = TTF_OpenFont("arial.ttf", 64);
    SDL_Color color = {0, 0, 255};
    title = TTF_RenderText_Blended(police, "Flying Shark", color);
    TTF_CloseFont(police);
    police = TTF_OpenFont("arial.ttf", 32);
    //Chargement des textes pour le menu
    entry[0] = TTF_RenderText_Blended(police, "Un joueur"           , color);
    entry[1] = TTF_RenderText_Blended(police, "Deux joueur"         , color);
    entry[2] = TTF_RenderText_Blended(police, "Options"             , color);
    entry[3] = TTF_RenderText_Blended(police, "Meilleurs scores"   , color);
    entry[4] = TTF_RenderText_Blended(police, "Quitter"             , color);
    //Declaration des constantes d'affichage
    int const margin = (system->screen->h - title->h - 32 * NB_ENTRY + BETWEEN_ENTRY * (NB_ENTRY - 1)) / 8 ;

    SDL_FillRect(system->screen, NULL, SDL_MapRGB(system->screen->format, 0, 0, 0));

    position.x = (system->screen->w - title->w ) / 2;
    position.y = margin;
    SDL_BlitSurface(title, NULL, system->screen, &position);

    for(i = 0; i < NB_ENTRY; i++)
    {
        int c = 0;
        SDL_Surface *tmp = NULL;

        for(c = 0; c < NB_ENTRY; c++)
        {
            tmp = entry[c];

            if(tmp->w > long_text)
                long_text = tmp->w;
        }

        tmp = entry[i];

        position.x = (system->screen->w - long_text / 2 - cursor->w) / 2;
        position.y = (margin * 2 + title->h + i * (BETWEEN_ENTRY + 32) + (32 - cursor->h) / 2);
        SDL_BlitSurface(tmp, NULL, system->screen, &position);
    }

    //Affichage du curseur
    position.x = ((system->screen->w - long_text / 2 - cursor->w) / 2) - cursor->w - 10;
    position.y = (margin * 2 + title->h + p_cursor * (BETWEEN_ENTRY + 32) + (32 - cursor->h) / 2);
    SDL_SetColorKey(cursor, SDL_SRCCOLORKEY, SDL_MapRGB(cursor->format, 255, 255, 255));
    SDL_BlitSurface(cursor, NULL, system->screen, &position);

    // Actualisation de l'ecran
    SDL_Flip(system->screen);
    
    // Dechargement des ressources
    SDL_FreeSurface(title);
    SDL_FreeSurface(cursor);
    SDL_FreeSurface(entry[0]);
    SDL_FreeSurface(entry[1]);
    SDL_FreeSurface(entry[2]);
    SDL_FreeSurface(entry[3]);
    SDL_FreeSurface(entry[4]);
    TTF_CloseFont(police);
}
