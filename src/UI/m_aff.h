/*
m_aff.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions pour l'affichage du menu
*/

#ifndef M_AFF_H_INCLUDED
#define M_AFF_H_INCLUDED

    void m_cursor_select();
    void m_aff(t_system *system, int p_cursor);

#endif
