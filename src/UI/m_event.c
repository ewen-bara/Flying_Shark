/*
m_event.c
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : inmplementation des fonctions defini dans m_event.h
*/

#include <SDL/SDL.h>


/* ####################################### */
/* Fonction qui sert a deplacer le curseur */
/* ####################################### */
void cursor_up(int *p_cursor)
{
    if(*p_cursor == 0)
        *p_cursor = 4;
    else
        *p_cursor -= 1;
}

/* ####################################### */
/* Fonction qui sert a deplacer le curseur */
/* ####################################### */
void cursor_down(int *p_cursor)
{
    if(*p_cursor < 4)
        *p_cursor += 1;
    else
        *p_cursor = 0;
}


/* ###################################### */
/* Fonction d'attente d'evenement du menu */
/* ###################################### */
int m_event(int *p_cursor)
{
    SDL_Event event;

    while(1)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                return 1;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_DOWN:
                        cursor_down(p_cursor);
                        return -1;
                    case SDLK_UP:
                        cursor_up(p_cursor);
                        return -1;
                    case SDLK_KP_ENTER:
                        return 1;
                    case SDLK_RETURN:
                        return 1;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    return 0;
}
