/*
m_event.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

R�le : d�finit les fonctions pour les evenement du menu
*/

#ifndef M_EVENT_H_INCLUDED
#define M_EVENT_H_INCLUDED

    void cursor_up(int *p_cursor);
    void cursor_down(int *p_cursor);
    int m_event(int *p_cursor);

#endif
