/*
m_sound.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : implementation des fonctions definis dans m_sound.h
*/

#include <stdio.h>
#include <stdlib.h>
#include <fmodex/fmod.h>

/* ################################## */
/* Fonction de lecture du son du menu */
/* ################################## */

void s_menu(FMOD_SYSTEM *system)
{

	FMOD_SOUND *music_menu;

	/* Chargement du son et verification du chargement */
	if(FMOD_System_CreateSound(system, "./sounds/menu.mp3", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &music_menu) != FMOD_OK)
	{
		fprintf(stderr, "Impossible de lire menu.mp3\n");
		exit(EXIT_FAILURE);
	}

	/* Activation de la repetition de la musique a l'infini */
	FMOD_Sound_SetLoopCount(music_menu, -1);

	/* Lecteur de la musique */
	FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, music_menu, 0, NULL);

	/* On libere le son, et on ferme et libere l'objet systeme */
    /* FMOD_Sound_Release(music_menu); */
}
