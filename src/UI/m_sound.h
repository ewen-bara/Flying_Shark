/*
m_sound.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions pour la gestion des sons dans le menu
*/

#ifndef M_SOUND_H_INCLUDED
#define M_SOUND_H_INCLUDED

	void s_menu(FMOD_SYSTEM *system);

#endif // M_SOUND_H_INCLUDED
