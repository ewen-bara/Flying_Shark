/*
menu.c
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : inmplementation des fonctions defini dans menu.h
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "../system.h"
#include "./m_aff.h"
#include "./m_event.h"
#include "./m_sound.h"


/* ######################### */
/* Fontion principal du menu */
/* ######################### */
int menu(t_system *system)
{
    //Declaration des variable d'etat
    int p_cursor = 0;
    int event = 0;

	//Appelle de la fonction jouant du son au menu
	//s_menu(system->sound);

    do
    {
        m_aff(system, p_cursor);
        event = m_event(&p_cursor);
    }while(event != 1);

    // Traitement de la position du curseur
    switch(p_cursor)
    {
    	case 0:
    		return 1;
    	case 1:
    		return 2;
    	case 2:
    		return 3;
    	case 3:
                return 4;
        case 4:
                return 0;
    	default:
    		return -1;
    }

    return -1;
}
