/*
menu.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions pour la gestion du menu
*/

#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

    int menu(t_system *system);

#endif // MENU_H_INCLUDED
