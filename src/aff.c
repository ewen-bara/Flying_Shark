/*
aff.c
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : inmplementation des fonctions defini dans aff.h
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#include "system.h"


/* ################################ */
/* Fonction qui initialise la video */
/* ################################ */
int init_screen(t_system *system)
{

    // Initialisation de la SDL
    if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
        fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
        return 1;
    }

    TTF_Init();

    // make sure SDL cleans up before exit
    atexit(SDL_Quit);

    SDL_WM_SetIcon(IMG_Load("./pictures/fshark.png"), NULL);

    // On cree une fenetre
    system->screen = SDL_SetVideoMode(800, 600, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);

    if (!system->screen) // On regarde si la creation de la fenetre a echouer ou non
    {
        fprintf(stderr, "Unable to set 800x600 video: %s\n", SDL_GetError());
        return 1;
    }

    SDL_WM_SetCaption("Flying Shark", NULL);

    return 0;
}
