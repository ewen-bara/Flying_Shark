/*
aff.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions pour l'initialisation de la fenetre
*/

#ifndef AFF_H_INCLUDED
#define AFF_H_INCLUDED

    int init_screen(t_system *system);
    int draw();

#endif // AFF_H_INCLUDED
