/*
audio.c
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : implementation des fonctions defini dans audio.h
*/

#include <stdio.h>
#include <stdlib.h>
#include <fmodex/fmod.h>

#include "system.h"

/* #################################### */
/* Fontion d'initialisation de l'audio  */
/* #################################### */

int init_audio(t_system *system)
{
    // Allocation et initialisation d'un objet systeme
    FMOD_System_Create(&system->sound);
    FMOD_System_Init(system->sound, 32, FMOD_INIT_NORMAL, NULL);
    return 0;
}

/* ############################### */
/* Fontion de fermeture de l'audio */
/* ############################### */

int clear_audio(t_system *system)
{
	// Fermeture et liberation d'un objet systeme
	FMOD_System_Close(system->sound);
	FMOD_System_Release(system->sound);
	return 0;
}

