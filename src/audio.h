/*
audio.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions pour la gestion de l'audio
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

 int init_audio(t_system *system);
 int clear_audio(t_system *system);

#endif // AUDIO_H_INCLUDED
