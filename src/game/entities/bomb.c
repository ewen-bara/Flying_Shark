﻿
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"

static void destruct_bomb(t_bomb **this)
{
        if(*this != NULL) {
                free(*this);
                *this=(t_bomb *)NULL;
        }
}


static void draw_bomb(t_bomb *this, t_system *system)
{
        SDL_BlitSurface(this->skin_bomb, NULL, system->screen, &(this->position_bomb));
}


static void explo_bomb(t_bomb *this, t_system *system)
{
        SDL_Rect position_explo;

        // Revoir les equations de positionnement
        position_explo.x = this->position_bomb.x+((this->skin_bomb->w)/2)-((this->skin_explo[this->last_skin_explo]->w)/2);
        position_explo.y = this->position_bomb.y+((this->skin_bomb->h)/2)-((this->skin_explo[this->last_skin_explo]->h)/2);//-250;

        this->skin_bomb = this->skin_explo[this->last_skin_explo];

        SDL_BlitSurface(this->skin_explo[this->last_skin_explo], NULL, system->screen, &position_explo);
        if(this->last_skin_explo < 1) // Nombre de skin-1  car sinon on sort sur tableau de skin
                this->last_skin_explo++;
        else
                this->destroyed_me = 1;
}


static void scroll_bomb(t_bomb *this, t_system *system, t_player *player)
{
        if((SDL_GetTicks() - (this->last_scroll) >= 2)){
                if(((this->pos_playertmp)-(this->position_bomb.y) < 250) && this->position_bomb.y > 0)
                        this->position_bomb.y--;
                else
                        this->explo(this, system);
                this->last_scroll = SDL_GetTicks();
        }
}


t_bomb *newbomb(t_player *player, t_res *res_data)
{
        int i = 0;

        t_bomb *this=(t_bomb *)malloc(sizeof(t_bomb));
        if (this==(t_bomb *)NULL)
                return((t_bomb *)NULL);

        /* affectation des methodes publiques */
        this->destruct  = destruct_bomb;

        this->draw      = draw_bomb;
        this->scroll    = scroll_bomb;
        this->explo     = explo_bomb;

        /* initialisation des attributs */
        this->id               = 0;
        this->destroyed_me     = 0;
        this->owner            = player;

        this->pos_playertmp   = player->position_player.y;

        this->skin_bomb       = res_data->bomb_skin;
        this->last_skin_explo = 0;

        for(i = 0; i < 11; i++)
                this->skin_explo[i]   = res_data->bomb_explo[i];

        this->position_bomb.x = player->position_player.x+((player->skin->w)/2)-((this->skin_bomb->w)/2);
        this->position_bomb.y = player->position_player.y-((player->skin->h)/2);

        /* retour de l'objet instancié */
        return(this);
}
