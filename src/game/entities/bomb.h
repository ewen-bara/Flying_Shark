/*
plane.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees des avion
*/

#ifndef BOMB_H_INCLUDED
#define BOMB_H_INCLUDED

#include <SDL/SDL.h>

#include "./entities.h"

 //typedef struct t_bomb t_bomb;

 struct t_bomb
 {
     //Definition des methodes
     void (*destruct)  (struct t_bomb **this);

     void (*draw)      (struct t_bomb *this, t_system *system);
     void (*scroll)    (struct t_bomb *this, t_system *system, t_player *player);
     void (*explo)     (struct t_bomb *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     int destroyed_me;
     t_player *owner;
     int pos_playertmp;

     SDL_Surface *skin_bomb;
     SDL_Surface *skin_explo[12];

     SDL_Rect position_bomb;

     Uint32 last_scroll;
     int    last_skin_explo;


 };

 extern t_bomb *newbomb(t_player *player, t_res *res_data);

#endif
