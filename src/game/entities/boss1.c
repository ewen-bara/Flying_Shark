﻿#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"

 static void destruct_boss1(t_boss1 **this)
{
    free(*this);
    *this=(t_boss1 *)NULL;
}


/*static void draw(t_boss1 *this, t_system *system)
{
    SDL_BlitSurface(this->skin_boss1, NULL, system->screen, &(this->position_boss1));
}*/


t_boss1 *newboss1(void)
{
    t_boss1 *this=(t_boss1 *)malloc(sizeof(t_boss1));
    if (this==(t_boss1 *)NULL)
        return((t_boss1 *)NULL);

    /* affectation des methodes publiques */
    this->destruct = destruct_boss1;

    /* initialisation des attributs */
    this->id         = 0;
    this->vie        = 8;
    this->point      = 2500;
    //this->skin_boss1       = IMG_Load("./pictures/boss1.bmp");

    /* retour de l'objet instancie */
    return(this);
}

