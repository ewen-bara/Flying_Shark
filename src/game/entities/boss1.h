/*
boss1.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees du boss1
*/

#ifndef BOSS1_H_INCLUDED
#define BOSS1_H_INCLUDED

#include <SDL/SDL.h>

 typedef struct t_boss1 t_boss1;

 struct t_boss1
 {
     //Definition des methodes
     void (*destruct)  (struct t_boss1 **this);
     //void (*draw)      (struct t_boss1 *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     int vie;
     int point;
     SDL_Surface *skin_boss1;
     SDL_Rect position_boss1;

 };

 extern t_boss1 *newboss1 (void);

#endif // BOSS1_H_INCLUDED

