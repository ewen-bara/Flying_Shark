#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"

 static void destruct_boss2(t_boss2 **this)
{
    free(*this);
    *this=(t_boss2 *)NULL;
}

/*static void draw(t_boss2 *this, t_system *system)
{
    SDL_BlitSurface(this->skin_boss2, NULL, system->screen, &(this->position_boss2));
}*/

t_boss2 *newboss2(void)
{
    t_boss2 *this=(t_boss2 *)malloc(sizeof(t_boss2));
    if (this==(t_boss2 *)NULL)
        return((t_boss2 *)NULL);

    /* affectation des m�thodes publiques */
    this->destruct = destruct_boss2;

    /* initialisation des attributs */
    this->id         = 0;
    this->vie        = 10;
    this->point      = 3000;
    //this->skin_boss2       = IMG_Load("./pictures/boss2.bmp");

    /* retour de l'objet instancie */
    return(this);
}

