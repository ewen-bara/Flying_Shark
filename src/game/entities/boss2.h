/*
boss2.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees du boss2
*/

#ifndef BOSS2_H_INCLUDED
#define BOSS2_H_INCLUDED

#include <SDL/SDL.h>

 typedef struct t_boss2 t_boss2;

 struct t_boss2
 {
     //Definition des methodes
     void (*destruct)  (struct t_boss2 **this);
     //void (*draw)      (struct t_boss2 *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     int vie;
     int point;
     SDL_Surface *skin_boss2;
     SDL_Rect position_boss2;

 };

 extern t_boss2 *newboss2 (void);

#endif // BOSS2_H_INCLUDED


