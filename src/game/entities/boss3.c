#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"

 static void destruct_boss3(t_boss3 **this)
{
    free(*this);
    *this=(t_boss3 *)NULL;
}

/*static void draw(t_boss3 *this, t_system *system)
{
    SDL_BlitSurface(this->skin_boss3, NULL, system->screen, &(this->position_boss3));
}*/

t_boss3 *newboss3(void)
{
    t_boss3 *this=(t_boss3 *)malloc(sizeof(t_boss3));
    if (this==(t_boss3 *)NULL)
        return((t_boss3 *)NULL);

    /* affectation des m�thodes publiques */
    this->destruct = destruct_boss3;

    /* initialisation des attributs */
    this->id         = 0;
    this->vie        = 12;
    this->point      = 3500;
    //this->skin_boss3       = IMG_Load("./pictures/boss3.bmp");

    /* retour de l'objet instancie */
    return(this);
}
