/*
boss3.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees du boss2
*/

#ifndef BOSS3_H_INCLUDED
#define BOSS3_H_INCLUDED

#include <SDL/SDL.h>

 typedef struct t_boss3 t_boss3;

 struct t_boss3
 {
     //Definition des methodes
     void (*destruct)  (struct t_boss3 **this);
     //void (*draw)      (struct t_boss3 *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     int vie;
     int point;
     SDL_Surface *skin_boss3;
     SDL_Rect position_boss3;

 };

 extern t_boss3 *newboss3 (void);

#endif // BOSS3_H_INCLUDED
