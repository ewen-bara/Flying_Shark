#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"

 static void destruct_boss4(t_boss4 **this)
{
    free(*this);
    *this=(t_boss4 *)NULL;
}

/*static void draw(t_boss4 *this, t_system *system)
{
    SDL_BlitSurface(this->skin_boss4, NULL, system->screen, &(this->position_boss4));
}*/

t_boss4 *newboss4(void)
{
    t_boss4 *this=(t_boss4 *)malloc(sizeof(t_boss4));
    if (this==(t_boss4 *)NULL)
        return((t_boss4 *)NULL);

    /* affectation des m�thodes publiques */
    this->destruct = destruct_boss4;

    /* initialisation des attributs */
    this->id         = 0;
    this->vie        = 15;
    this->point      = 4500;
    //this->skin_boss4       = IMG_Load("./pictures/boss4.bmp");

    /* retour de l'objet instancie */
    return(this);
}


