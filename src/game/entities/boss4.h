/*
boss4.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees du boss4
*/

#ifndef BOSS4_H_INCLUDED
#define BOSS4_H_INCLUDED

#include <SDL/SDL.h>

 typedef struct t_boss4 t_boss4;

 struct t_boss4
 {
     //Definition des methodes
     void (*destruct)  (struct t_boss4 **this);
     //void (*draw)      (struct t_boss4 *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     int vie;
     int point;
     SDL_Surface *skin_boss4;
     SDL_Rect position_boss4;

 };

 extern t_boss4 *newboss4 (void);

#endif // BOSS3_H_INCLUDED
