#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

//#include "./entities.h"
#include "./boss5.h"

 static void destruct_boss5(t_boss5 **this)
{
    free(*this);
    *this=(t_boss5 *)NULL;
}

/*static void draw(t_boss5 *this, t_system *system)
{
    SDL_BlitSurface(this->skin_boss5, NULL, system->screen, &(this->position_boss5));
}*/

t_boss5 *newboss5(void)
{
    t_boss5 *this=(t_boss5 *)malloc(sizeof(t_boss5));
    if (this==(t_boss5 *)NULL)
        return((t_boss5 *)NULL);

    /* affectation des m�thodes publiques */
    this->destruct = destruct_boss5;

    /* initialisation des attributs */
    this->id         = 0;
    this->vie        = 20;
    this->point      = 6000;
    //this->skin_boss5       = IMG_Load("./pictures/boss5.bmp");

    /* retour de l'objet instancie */
    return(this);
}

