/*
boss3.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees du boss5
*/

#ifndef BOSS3_H_INCLUDED
#define BOSS3_H_INCLUDED

#include <SDL/SDL.h>

 typedef struct t_boss5 t_boss5;

 struct t_boss5
 {
     //Definition des methodes
     void (*destruct)  (struct t_boss5 **this);
     //void (*draw)      (struct t_boss5 *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     int vie;
     int point;
     SDL_Surface *skin_boss5;
     SDL_Rect position_boss5;

 };

 extern t_boss5 *newboss5 (void);

#endif // BOSS5_H_INCLUDED
