#ifndef ENTITIES_H_INCLUDED
#define ENTITIES_H_INCLUDED

 typedef struct t_player   t_player;
 typedef struct t_shot     t_shot;
 typedef struct t_bomb     t_bomb;
 typedef struct t_opponent t_opponent;

 #include "./player.h"
 #include "./shot.h"
 #include "./bomb.h"
 #include "./opponent.h"
 #include "./plane.h"
 #include "./tank.h"
 #include "./boss1.h"
 #include "./boss2.h"
 #include "./boss3.h"
 #include "./boss4.h"
 #include "./boss5.h"

#endif
