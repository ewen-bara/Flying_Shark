/*
plane.c
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees des avion
*/

#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"


static void destruct_opponent(t_opponent **this)
{
    free(*this);
    *this=(t_opponent *)NULL;
}


static void draw_opponent(t_opponent *this, t_system *system)
{
    SDL_BlitSurface(this->skin, NULL, system->screen, &(this->position));
}

t_opponent *newopponent(t_res *res_data)
{
    t_opponent *this=(t_opponent *)malloc(sizeof(t_opponent));
    if (this==(t_opponent *)NULL)
        return((t_opponent *)NULL);

    /* affectation des methodes publiques */
    this->destruct = destruct_opponent;

    this->draw     = draw_opponent;

    /* initialisation des attributs */
    this->id         = 250;
    this->skin = NULL;

    /* retour de l'objet instancie */
    return(this);
}
