/*
 * opponent.h
 * ------------
 *
 * Par Bara Ewen, pour le projet "Flying Shark"
 *
 * Role :
 */

#ifndef OPPONENT_H_INCLUDED
#define OPPONENT_H_INCLUDED

#include <SDL/SDL.h>

#include "../../system.h"
#include "../../res.h"
//#include "./shot.h"
//#include "./bomb.h"


// typedef struct t_player t_player;

// Class mere de plane, tank et les boss

struct t_opponent
{
	//Definition des methodes
	void (*destruct)  (struct t_opponent **this);
	void (*draw)      (struct t_opponent *this, t_system *system);

	void (*shoot)     (struct t_opponent *this, t_system *system, t_res *res_data);
	void (*shoot2)    (struct t_opponent *this, t_system *system, t_res *res_data);

	//Definition des variables de l'entite
	int id;
	SDL_Surface *skin;
	SDL_Rect position;
	Uint32 last_shoot;

	t_shot* mem_shot[1024];
};

extern t_opponent *newopponent(t_res *res_data); // Theoriquement inutile

#endif // OPPONENT_H_INCLUDED

