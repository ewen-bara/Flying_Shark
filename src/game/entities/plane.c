﻿/*
 * plane.c
 * ------------
 *
 * Par Bara Ewen, pour le projet "Flying Shark"
 *
 * Role : definit les fonctions de traitement des donnees des avion
 */

#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"


static void destruct_plane(t_plane **this)
{
        free(*this);
        *this=(t_plane *)NULL;
}


static void draw_plane(t_plane *this, t_system *system)
{
        SDL_BlitSurface(this->skin, NULL, system->screen, &(this->position));
}


t_plane *newplane(t_res *res_data)
{
        t_plane *this=(t_plane *)malloc(sizeof(t_plane));
        if (this==(t_plane *)NULL)
                return((t_plane *)NULL);

        /* affectation des methodes publiques */
        ((t_opponent*)this)->destruct = destruct_plane;

        ((t_opponent*)this)->draw     = draw_plane;

        /* initialisation des attributs */
        ((t_opponent*)this)->id         = 250;
        //this->point      = 250;
        ((t_opponent*)this)->skin = res_data->plane_skin;

        SDL_SetColorKey(((t_opponent*)this)->skin, SDL_SRCCOLORKEY, SDL_MapRGB(((t_opponent*)this)->skin->format, 255, 255, 255));

        /* retour de l'objet instancie */
        return(this);
}
