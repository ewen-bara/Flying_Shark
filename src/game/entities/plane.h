﻿/*
 * plane.h
 * ------------
 *
 * Par Bara Ewen, pour le projet "Flying Shark"
 *
 * Role : definit les fonctions de traitement des donnees des avion
 */

#ifndef PLANE_H_INCLUDED
#define PLANE_H_INCLUDED

#include <SDL/SDL.h>

#include "./opponent.h"

typedef struct t_plane t_plane;

struct t_plane
{
        struct t_opponent parent;

        //Definition des methodes
        void (*destruct)  (struct t_plane **this);
        void (*draw)      (struct t_plane *this, t_system *system);

        void (*shoot)     (struct t_player *this, t_system *system, t_res *res_data);
        void (*shoot2)    (struct t_player *this, t_system *system, t_res *res_data);

        //Definition des variables de l'entite
        int id;
        SDL_Surface *skin;
        SDL_Rect position;
        Uint32 last_shoot;

        t_shot* mem_shot[1024];
};

extern t_plane *newplane(t_res *res_data);

#endif // PLANE_H_INCLUDED
