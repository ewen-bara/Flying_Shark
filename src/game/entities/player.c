﻿/*
 * player.c
 * ------------
 *
 * Par Bara Ewen, pour le projet "Flying Shark"
 *
 * Role : implementation des fonctions definit dans player.h
 */

#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"
#include "../g_sound.h"


static void destruct_player(t_player **this)
{
        if(*this != NULL) {
                free(*this);
                *this=(t_player *)NULL;
        }
}


static void player_move_up(t_player *this, t_system *system)
{
        if (this->position_player.y != 0 && (SDL_GetTicks() - (this->last_up) >= 1)) {
                this->position_player.y--;
                this->last_up = SDL_GetTicks();
        }
}


static void player_move_down(t_player *this, t_system *system)
{
        if ((this->position_player.y +32< system->screen->h) && (SDL_GetTicks() - (this->last_down) >= 1)) {
                this->position_player.y++;
                this->last_down = SDL_GetTicks();
        }
}


static void player_move_right(t_player *this, t_system *system)
{
        if ((this->position_player.x +32 < system->screen->w) && (SDL_GetTicks() - (this->last_right) >= 1)) {
                this->position_player.x++;
                this->last_right = SDL_GetTicks();
        }

        this->skin = this->skin_right;
}


static void player_move_left(t_player *this, t_system *system)
{
        if ((this->position_player.x != 0) && (SDL_GetTicks() - (this->last_left) >= 1)) {
                (this->position_player).x--;
                this->last_left = SDL_GetTicks();
        }

        this->skin = this->skin_left;
}


static void player_shoot(t_player *this, t_system *system, t_res *res_data)
{
        int i = 0;

        // 4 tirs par seconde
        if((SDL_GetTicks() - (this->last_shoot)) >= 250) {
                while(i < 1024) {
                        if(this->mem_shot[i] == NULL) {
                                this->mem_shot[i] = newshot(this, res_data);
                                s_playershoot(system->sound, res_data);
                                break;
                        }
                        i++;
                }

                this->last_shoot = SDL_GetTicks();

        }

}


static void player_shoot2(t_player *this, t_system *system, t_res *res_data)
{
        int i = 0;

        // 1 bombe par seconde
        if((SDL_GetTicks() - (this->last_bomb) >= 1000) && (this->nb_bomb > 0)) {
                while(i < 10) {
                        if(this->mem_bomb[i] == NULL) {
                                this->mem_bomb[i] = newbomb(this, res_data);
                                this->nb_bomb--;
                                s_playershoot2(system->sound, res_data);
                                break;
                        }
                        i++;
                }

                this->last_bomb = SDL_GetTicks();
        }
}


static void draw_player(t_player *this, t_system *system)
{
        SDL_BlitSurface(this->skin, NULL, system->screen, &(this->position_player));
}


t_player *newplayer(t_res *res_data)
{
        int i = 0;

        t_player *this=(t_player *)malloc(sizeof(t_player));
        if (this==(t_player *)NULL)
                return((t_player *)NULL);

        /* affectation des méthodes publiques */
        this->destruct = destruct_player;

        this->move_up    = player_move_up;
        this->move_down  = player_move_down;
        this->move_right = player_move_right;
        this->move_left  = player_move_left;
        this->shoot      = player_shoot;
        this->shoot2     = player_shoot2;
        this->draw       = draw_player;

        /* initialisation des attributs */
        this->id         = 0;
        this->nb_life    = 3;
        this->nb_bomb    = 50;
        this->score      = 0;
        this->skin_face  = res_data->player_face;
        this->skin_left  = res_data->player_left;
        this->skin_right = res_data->player_right;
        this->skin       = this->skin_face;
        this->position_player.x = 250;
        this->position_player.y = 250;

        for(i = 0; i < 1024; i++)
                this->mem_shot[i] = (t_shot*)NULL;

        for(i = 0; i < 10; i++)
                this->mem_bomb[i] = (t_bomb*)NULL;

        /* retour de l'objet instancié */
        return(this);
}
