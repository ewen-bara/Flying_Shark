﻿/*
player.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees du joueur
*/

#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <SDL/SDL.h>

#include "../../system.h"
#include "../../res.h"
#include "./shot.h"
#include "./bomb.h"


// typedef struct t_player t_player;

 struct t_player
 {
     //Definition des methodes
     void (*destruct)  (struct t_player **this);

     void (*move_up)   (struct t_player *this, t_system *system);
     void (*move_down) (struct t_player *this, t_system *system);
     void (*move_right)(struct t_player *this, t_system *system);
     void (*move_left) (struct t_player *this, t_system *system);
     void (*shoot)     (struct t_player *this, t_system *system, t_res *res_data);
     void (*shoot2)    (struct t_player *this, t_system *system, t_res *res_data);
     void (*draw)      (struct t_player *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     SDL_Surface *skin;
     SDL_Surface *skin_face;
     SDL_Surface *skin_left;
     SDL_Surface *skin_right;
     int nb_life;
     int nb_bomb;
     int score;
     SDL_Rect position_player;
     Uint32 last_shoot;
     Uint32 last_bomb;
     Uint32 last_up;
     Uint32 last_down;
     Uint32 last_right;
     Uint32 last_left;

     t_shot* mem_shot[1024];
     t_bomb* mem_bomb[10];

 };

 extern t_player *newplayer(t_res *res_data);

#endif // PLAYER_H_INCLUDED
