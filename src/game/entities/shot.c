﻿/*
 * shot.c
 * ------------
 *
 * Par Bara Ewen, pour le projet "Flying Shark"
 *
 * Role : implementation des fonctions definit dans shot.h
 */

#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"
#include "../../res.h"


static void destruct_shot(t_shot **this)
{
        free(*this);
        *this=(t_shot *)NULL;
}


static void draw_shot(t_shot *this, t_system *system)
{
        SDL_BlitSurface(this->skin_shot, NULL, system->screen, &(this->position_shot));
}


static void scroll_shot(t_shot *this, t_system *system, t_player *player)
{
        if(this->position_shot.y >= 0 && (SDL_GetTicks() - (this->last_scroll) >= 1))
        {
                if(this->position_shot.y > 0)
                        this->position_shot.y--;
                //else
                //    this->position_shot2.y++;

                if(this->position_shot.y <= 0)
                        this->destroyed_me = 1;

                this->last_scroll = SDL_GetTicks();
        }
}


t_shot *newshot(t_player *player, t_res *res_data)
{
        t_shot *this=(t_shot *)malloc(sizeof(t_shot));
        if (this==(t_shot *)NULL)
                return((t_shot *)NULL);

        /* affectation des methodes publiques */
        this->destruct = destruct_shot;

        this->draw     = draw_shot;
        this->scroll   = scroll_shot;

        /* initialisation des attributs */
        this->id               = 0;
        this->destroyed_me     = 0;
        this->owner            = player;
        this->skin_shot        = res_data->shot_skin;
        this->position_shot.x  = player->position_player.x+((player->skin->w)/2)-((this->skin_shot->w)/2);
        this->position_shot.y  = player->position_player.y-((player->skin->h)/2);
        //    this->position_shot2.x = 0;
        //    this->position_shot2.y = 0;

        //SDL_SetColorKey(this->skin_shot, SDL_SRCCOLORKEY, SDL_MapRGB(this->skin_shot->format, 255, 255, 255));

        /* retour de l'objet instancie */
        return(this);
}
