/*
shot.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees des avion
*/

#ifndef SHOT_H_INCLUDED
#define SHOT_H_INCLUDED

#include <SDL/SDL.h>

#include "./entities.h"
#include "../../res.h"

// typedef struct t_shot t_shot;

 struct t_shot
 {
     //Definition des methodes
     void (*destruct)  (struct t_shot **this);

     void (*draw)      (struct t_shot *this, t_system *system);
     void (*scroll)    (struct t_shot *this, t_system *system, t_player *player);

     //Definition des variables de l'entite
     int id;
     int destroyed_me;
     t_player *owner;
     SDL_Surface *skin_shot;
     SDL_Rect position_shot;
     SDL_Rect position_shot2;

     Uint32 last_scroll;

 };

 extern t_shot *newshot(t_player *player, t_res *res_data);

#endif
