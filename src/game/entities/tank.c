/*
tank.c
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees des tanks
*/

#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./entities.h"


static void destruct_tank(t_tank **this)
{
    free(*this);
    *this=(t_tank *)NULL;
}


/*static void draw(t_tank *this, t_system *system)
{
    SDL_BlitSurface(this->skin_tank, NULL, system->screen, &(this->position_tank));
}*/


t_tank *newtank(void)
{
    t_tank *this=(t_tank *)malloc(sizeof(t_tank));
    if (this==(t_tank *)NULL)
        return((t_tank *)NULL);

    /* affectation des methodes publiques */
    this->destruct = destruct_tank;

    /* initialisation des attributs */
    this->id         = 0;
    this->point      = 200;
    //this->skin_tank = IMG_Load("./pictures/tank.bmp");


    /* retour de l'objet instancie */
    return(this);
}
