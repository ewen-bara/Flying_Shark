/*
tank.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees des avion
*/

#ifndef TANK_H_INCLUDED
#define TANK_H_INCLUDED

#include <SDL/SDL.h>

 typedef struct t_tank t_tank;

 struct t_tank
 {
     //Definition des methodes
     void (*destruct)  (struct t_tank **this);
     //void (*draw)      (struct t_tank *this, t_system *system);

     //Definition des variables de l'entite
     int id;
     int point;
     SDL_Surface *skin_tank;
     SDL_Rect position_tank;

 };

 extern t_tank *newtank(void);

#endif // TANK_H_INCLUDED
