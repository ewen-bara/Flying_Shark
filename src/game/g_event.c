﻿/*
g_event.c
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : implementation des fonctions definit dans g_event.h
*/

#include <SDL/SDL.h>

#include "../system.h"
#include "./g_sound.h"
#include "./entities/entities.h"
#include "map.h"


int g_event(t_player *player, t_system *system, t_res *res_data, t_map *g_data)
{
    // Permet de vider le buffer des events sdl
    SDL_Event event;
    SDL_PollEvent(&event);

    Uint8 *keystate = SDL_GetKeyState(NULL);
    SDL_PumpEvents();

    if(keystate[SDLK_RETURN] || keystate[SDLK_ESCAPE])
        g_data->quit = 1;

    if(keystate[SDLK_DOWN])
        player->move_down(player, system);

    if(keystate[SDLK_UP])
        player->move_up(player, system);

    if(keystate[SDLK_RIGHT])
        player->move_right(player, system);

    if(keystate[SDLK_LEFT])
        player->move_left(player,system);

    if((!keystate[SDLK_LEFT] && !keystate[SDLK_RIGHT]) || (keystate[SDLK_LEFT] && keystate[SDLK_RIGHT]))
        player->skin = player->skin_face;

    if(keystate[SDLK_RCTRL])
        player->shoot(player, system, res_data);

    if(keystate[SDLK_KP0])
        player->shoot2(player, system, res_data);

    if(keystate[SDLK_h] && keystate[SDLK_RCTRL])
        s_headshot(system->sound, res_data);

    return 0;
}


// Fonction d'evenement temporaire (Mettre dans la structure du joueur la configuration des touche)
int g_event2(t_player *player, t_system *system, t_res *res_data, t_map *g_data)
{
    // Permet de vider le buffer des events sdl
    SDL_Event event;
    SDL_PollEvent(&event);

    Uint8 *keystate = SDL_GetKeyState(NULL);
    SDL_PumpEvents();

    //if(keystate[SDLK_RETURN] || keystate[SDLK_ESCAPE])
    //    g_data->quit = 1;

    if(keystate[SDLK_s])
        player->move_down(player, system);

    if(keystate[SDLK_z])
        player->move_up(player, system);

    if(keystate[SDLK_d])
        player->move_right(player, system);

    if(keystate[SDLK_q])
        player->move_left(player,system);

    if((!keystate[SDLK_q] && !keystate[SDLK_d]) || (keystate[SDLK_q] && keystate[SDLK_d]))
        player->skin = player->skin_face;

    if(keystate[SDLK_LCTRL])
        player->shoot(player, system, res_data);

    if(keystate[SDLK_LALT])
        player->shoot2(player, system, res_data);

    return 0;
}
