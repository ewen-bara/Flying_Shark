/*
g_event.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions des evenements d'une partie
*/

#ifndef G_EVENT_H_INCLUDED
#define G_EVENT_H_INCLUDED

 int g_event(t_player *player, t_system *system, t_res *res_data, t_map *g_data);
 int g_event2(t_player *player, t_system *system, t_res *res_data, t_map *g_data);

#endif
