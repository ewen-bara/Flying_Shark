/*
g_sound.c
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : implementation des fonctions definis dans g_sound.h
*/

#include <stdio.h>
#include <stdlib.h>
#include <fmodex/fmod.h>

#include "../res.h"

/* ######################################## */
/* Fonction de lecture du son de tir (tir1)*/
/* ######################################## */

void s_playershoot(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 1, res_data->sound_playershoot, 0, NULL);

}

/* ######################################### */
/* Fonction de lecture du tir special (tir2) */
/* ######################################### */

void s_playershoot2(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 2, res_data->sound_playershoot2, 0, NULL);

}

/* ################################################## */
/* Fonction de lecture du son de crash ennemis (tir1) */
/* ################################################## */

void s_crash1(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 3, res_data->sound_crash1, 0, NULL);

}

/* ##################################################### */
/* Fonction de lecture du son d'explosion ennemis (tir2) */
/* ##################################################### */

void s_crash2(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 3, res_data->sound_crash2, 0, NULL);

}

/* ####################################### */
/* Fonction de lecture du son de bonus vie */
/* ####################################### */

void s_bonuslife(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 4, res_data->sound_bonuslife, 0, NULL);

}

/* ######################################### */
/* Fonction de lecture du son de bonus tir 1 */
/* ######################################### */

void s_bonusshoot1(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 5, res_data->sound_bonusshoot1, 0, NULL);

}

/* ######################################### */
/* Fonction de lecture du son de bonus tir 2 */
/* ######################################### */

void s_bonusshoot2(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 6, res_data->sound_bonusshoot2, 0, NULL);

}

/* ###################################### */
/* Fonction de lecture du son du gameover */
/* ###################################### */

void s_gameover(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 0, res_data->sound_gameover, 0, NULL);

}

/* ######################################### */
/* Fonction de lecture du son d'intro level1 */
/* ######################################### */

 void s_levelintro(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 0, res_data->sound_levelintro, 0, NULL);

 }

/* ##################################### */
/* Fonction de lecture du son du level 1 */
/* ##################################### */

 void s_level1(FMOD_SYSTEM *system, t_res *res_data) {

	/* Repetition de la musique */
	FMOD_Sound_SetLoopCount(res_data->sound_level1, -1);

	/* Lecture du son */
	FMOD_System_PlaySound(system, 0, res_data->sound_level1, 0, NULL);

 }

/* ##################################### */
/* Fonction de lecture du son du level 2 */
/* ##################################### */

 void s_level2(FMOD_SYSTEM *system, t_res *res_data) {

	/* Repetition de la musique */
	FMOD_Sound_SetLoopCount(res_data->sound_level2, -1);

	/* Lecture du son */
	FMOD_System_PlaySound(system, 0, res_data->sound_level2, 0, NULL);

 }

/* ##################################### */
/* Fonction de lecture du son du level 3 */
/* ##################################### */

 void s_level3(FMOD_SYSTEM *system, t_res *res_data) {

	/* Repetition de la musique */
	FMOD_Sound_SetLoopCount(res_data->sound_level3, -1);

	/* Lecture du son */
	FMOD_System_PlaySound(system, 0, res_data->sound_level3, 0, NULL);

 }

/* ##################################### */
/* Fonction de lecture du son du level 4 */
/* ##################################### */

 void s_level4(FMOD_SYSTEM *system, t_res *res_data) {

	/* Repetition de la musique */
	FMOD_Sound_SetLoopCount(res_data->sound_level4, -1);

	/* Lecture du son */
	FMOD_System_PlaySound(system, 0, res_data->sound_level4, 0, NULL);

 }

/* ##################################### */
/* Fonction de lecture du son du level 5 */
/* ##################################### */

 void s_level5(FMOD_SYSTEM *system, t_res *res_data) {

	/* Repetition de la musique */
	FMOD_Sound_SetLoopCount(res_data->sound_level5, -1);
	/* Lecture du son */
	FMOD_System_PlaySound(system, 0, res_data->sound_level5, 0, NULL);

 }





 void s_headshot(FMOD_SYSTEM *system, t_res *res_data) {

	/* Lecture du son */
	FMOD_System_PlaySound(system, 31, res_data->sound_headshot, 0, NULL);

 }
