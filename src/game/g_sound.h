/*
g_sound.h
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : definit les fonctions pour la gestion des son du jeu
*/

#ifndef G_SOUND_H_INCLUDED
#define G_SOUND_H_INCLUDED

#include "../res.h"

 void s_playershoot(FMOD_SYSTEM *system, t_res *res_data);
 void s_playershoot2(FMOD_SYSTEM *system, t_res *res_data);
 void s_crash1(FMOD_SYSTEM *system, t_res *res_data);
 void s_crash2(FMOD_SYSTEM *system, t_res *res_data);
 void s_bonuslife(FMOD_SYSTEM *system, t_res *res_data);
 void s_bonusshoot1(FMOD_SYSTEM *system, t_res *res_data);
 void s_bonusshoot2(FMOD_SYSTEM *system, t_res *res_data);
 void s_gameover(FMOD_SYSTEM *system, t_res *res_data);
 void s_levelintro(FMOD_SYSTEM *system, t_res *res_data);
 void s_level1(FMOD_SYSTEM *system, t_res *res_data);
 void s_level2(FMOD_SYSTEM *system, t_res *res_data);
 void s_level3(FMOD_SYSTEM *system, t_res *res_data);
 void s_level4(FMOD_SYSTEM *system, t_res *res_data);
 void s_level5(FMOD_SYSTEM *system, t_res *res_data);
 void s_headshot(FMOD_SYSTEM *system, t_res *res_data);

#endif // G_SOUND_H_INCLUDED
