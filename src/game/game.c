﻿/*
 * game.c
 * ------------
 *
 * Par Bara Ewen, pour le projet "Flying Shark"
 *
 * Role : implementation des fonctions definit dans game.h
 */

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "../system.h"
#include "../res.h"
#include "./entities/entities.h"
#include "./map.h"
#include "./level.h"
#include "./g_event.h"


void scroll_map(t_system *system, t_map *g_data)
{
        // le temps minimal entre deux changement de position doit etre egal au temps pour les ips (pour garder la fluidite du jeu)
        if((SDL_GetTicks() - (g_data->last_scroll)) >= 17) {
                g_data->position_back.y -= 2;
                SDL_FillRect(system->screen, NULL, SDL_MapRGB(system->screen->format, 0, 0, 0));
                SDL_BlitSurface(g_data->background, &(g_data->position_back), system->screen, NULL);
                g_data->last_scroll = SDL_GetTicks();
        }
}

void routine(t_system *system, t_res *res_data, t_map *g_data, t_player *player)
{
        int i;

        // Routine des tirs du joueur
        for (i = 0; i < 1024; i++) {
                if (player->mem_shot[i] != NULL) {
                        player->mem_shot[i]->draw(player->mem_shot[i], system);
                        player->mem_shot[i]->scroll(player->mem_shot[i], system, player);
                        if(player->mem_shot[i]->destroyed_me == 1)
                                player->mem_shot[i]->destruct(&(player->mem_shot[i]));
                }
        }

        // Routine des bombes du joueur
        for (i = 0; i < 10; i++) {
                if (player->mem_bomb[i] != NULL) {
                        player->mem_bomb[i]->draw(player->mem_bomb[i], system);
                        player->mem_bomb[i]->scroll(player->mem_bomb[i], system, player);

                        if(player->mem_bomb[i]->destroyed_me == 1)
                                player->mem_bomb[i]->destruct(&(player->mem_bomb[i]));
                }
        }
}

void game(t_system *system, int nb_player)
{
        // Creation des ressources
        t_res res_data;

        // Chargement des ressources
        load_res(system, &res_data);

        // Creation et initialisation des v&(g_data->position_back)ariables du jeu
        t_map g_data;
        t_player *player1 = newplayer(&res_data);
        t_player *player2 = newplayer(&res_data);

        g_data.quit = 0;

        do {
                // Initialisation des fonctions et variables du level
                g_data.level_init = level1_init;
                g_data.level      = level1;

                g_data.level_init(system, &g_data, &res_data);

                g_data.position_back.x = 0;
                g_data.position_back.y = g_data.background->h - system->screen->h;
                g_data.position_back.w = system->screen->w;
                g_data.position_back.h = system->screen->h;

                do {
                        // Execution des routines du jeu
                        scroll_map(system, &g_data);
                        g_data.level(system, &g_data);

                        // Si on est en mode 2 joueurs on executer les routines du joueurs 2
                        if(nb_player == 2) {
                                routine(system, &res_data, &g_data, player2);
                                player2->draw(player2, system);
                                g_event2(player2, system, &res_data, &g_data);
                        }

                        // Execution des routine du joueur 1
                        routine(system, &res_data, &g_data, player1);
                        player1->draw(player1, system);
                        g_event(player1, system, &res_data, &g_data);


                        // Actualisation de l'ecran pour avoir 60 ips environ
                        if((SDL_GetTicks() - (g_data.last_flip)) >= 17) {
                                SDL_Flip(system->screen);
                                g_data.last_flip = SDL_GetTicks();
                        }

                        // Empeche l'utilisation complete d'un coeur (un option doit etre rajouter pour le desactiver)
                        SDL_Delay(1);

                }while(g_data.quit != 1);

                /*
                 * Traitement des données de fin de level. ( a creer)
                 */
        }while(g_data.quit !=1);

        // Destruction des joueurs
        player1->destruct(&player1);
        player2->destruct(&player2);

        // Dechargement des ressources
        unload_res(&res_data);
}
