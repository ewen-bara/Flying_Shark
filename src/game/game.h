/*
game.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees de la partie
*/

#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "map.h"

    void scroll_map(t_system *system, t_map *g_data);
    void game(t_system *system, int nb_player);

#endif
