/*
level1.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : definit les fonctions de traitement des donnees de la partie
*/

#ifndef LEVEL1_H_INCLUDED
#define LEVEL1_H_INCLUDED

 void level1_init(t_system *system, t_map *g_data, t_res *res_data);
 void level1(t_system *system, t_map *g_data, t_res *res_data);

#endif
