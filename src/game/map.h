/*
 * File:   map.h
 * Author: mstislav
 *
 * Created on 6 juin 2012, 10:12
 */

#ifndef MAP_H
#define	MAP_H

#include <SDL/SDL.h>

#include "../res.h"

 typedef struct t_map t_map;

 struct t_map
 {
     void (*level_init) (t_system *system, struct t_map *g_data, t_res *res_data);
     void (*level)      (t_system *system, t_map *g_data);

     int          quit;
     SDL_Surface *background;
     SDL_Rect     position_back;
     Uint32       last_scroll;
     Uint32       last_flip;
     void        *opponents[1024];
 };

#endif	/* MAP_H */
