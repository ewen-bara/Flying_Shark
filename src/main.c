﻿#include <stdlib.h>
#include <SDL/SDL.h>

#include "system.h"
#include "aff.h"
#include "audio.h"
#include "./UI/menu.h"
#include "./game/game.h"


//int main(int argc, char *argv[])
int main(void)
{
        // Declaration de la variable pour l'audio et la video
        t_system system;

        // Initialisation de la variable system
        if(init_screen(&system) || init_audio(&system))
                return 1;

        // Menu desactiver car il provoque un bug pour le scrolling en jeu
        do {
                switch(menu(&system)) { // Appel du menu et traitement du retour de la fonction
                        case 0:
                                // On quitte le programme
                                exit_prog(&system);
                                break;
                        case 1:
                                game(&system, 1);
                                break;
                        case 2:
                                game(&system, 2);
                                break;
                        case 3:
                                // options
                        case 4:
                                // highscore
                        default:
                                break;
                }
        }while(1); // Boucle infini (une fontion gere la sortie du jeu)


        // Appel temporaire de la fonction game (cause: bug provoquer par le menu)
        //game(&system, 1);

        // Appelle de la fonction fermant le programme proprement
        exit_prog(&system);

        return 0;
}
