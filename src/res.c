﻿/*
 * File:   res.c
 * Author: mstislav
 *
 * Created on 7 juin 2012, 09:35
 */

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "./system.h"
#include "./res.h"

void load_res(t_system *system, t_res *res_data)
{
        int i;

        /* ##################### */
        /* Chargement des images */
        /* ##################### */

        res_data->cursor          = IMG_Load("./pictures/cursor.bmp");
        res_data->player_face     = IMG_Load("./pictures/player/fshark.bmp");
        res_data->player_right    = IMG_Load("./pictures/player/fshark_right.bmp");
        res_data->player_left     = IMG_Load("./pictures/player/fshark_left.bmp");
        res_data->shot_skin       = IMG_Load("./pictures/player/weapons/shot.bmp");
        res_data->bomb_skin       = IMG_Load("./pictures/player/weapons/bomb.bmp");

        res_data->bomb_explo[0]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb1.bmp");
        res_data->bomb_explo[1]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb2.bmp");
        res_data->bomb_explo[2]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb3.bmp");
        res_data->bomb_explo[3]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb4.bmp");
        res_data->bomb_explo[4]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb5.bmp");
        res_data->bomb_explo[5]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb6.bmp");
        res_data->bomb_explo[6]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb7.bmp");
        res_data->bomb_explo[7]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb8.bmp");
        res_data->bomb_explo[8]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb9.bmp");
        res_data->bomb_explo[9]   = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb10.bmp");
        res_data->bomb_explo[10]  = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb11.bmp");
        res_data->bomb_explo[11]  = IMG_Load("./pictures/player/weapons/explo_bomb/explo_bomb12.bmp");

        res_data->plane_skin      = IMG_Load("./pictures/opponents/plane.bmp");
        res_data->level1          = IMG_Load("./pictures/levels/level1.png");
        res_data->level2          = IMG_Load("./pictures/levels/level1.png");
        res_data->level3          = IMG_Load("./pictures/levels/level1.png");
        res_data->level4          = IMG_Load("./pictures/levels/level1.png");
        res_data->level5          = IMG_Load("./pictures/levels/level1.png");

        /* ############################ */
        /* Chargement des transparences */
        /* ############################ */

        SDL_SetColorKey(res_data->cursor, SDL_SRCCOLORKEY, SDL_MapRGB(res_data->cursor->format, 255, 255, 255));
        SDL_SetColorKey(res_data->player_face, SDL_SRCCOLORKEY, SDL_MapRGB(res_data->player_face->format, 255, 255, 255));
        SDL_SetColorKey(res_data->player_right, SDL_SRCCOLORKEY, SDL_MapRGB(res_data->player_right->format, 255, 255, 255));
        SDL_SetColorKey(res_data->player_left, SDL_SRCCOLORKEY, SDL_MapRGB(res_data->player_left->format, 255, 255, 255));
        SDL_SetColorKey(res_data->shot_skin, SDL_SRCCOLORKEY, SDL_MapRGB(res_data->shot_skin->format, 255, 255, 255));
        SDL_SetColorKey(res_data->bomb_skin, SDL_SRCCOLORKEY, SDL_MapRGB(res_data->bomb_skin->format, 255, 255, 255));

        for(i = 0; i < 2; i++) // Deux images existantes
                SDL_SetColorKey(res_data->bomb_explo[i], SDL_SRCCOLORKEY, SDL_MapRGB(res_data->bomb_explo[i]->format, 255, 255, 255));

        SDL_SetColorKey(res_data->plane_skin, SDL_SRCCOLORKEY, SDL_MapRGB(res_data->plane_skin->format, 255, 255, 255));


        /* ############################# */
        /* Chargement des fichiers audio */
        /* ############################# */


        if (FMOD_System_CreateSound(system->sound, "./sounds/Playershoot1.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_playershoot)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire Playershoot1.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/Playershoot2.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_playershoot2)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire Playershoot2.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/FighterCrash.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_crash1)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire FighterCrash.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/FighterExplosion.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_crash2)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire FighterExplosion.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/BonusLife.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_bonuslife)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire BonusLife.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/BonusTir1.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_bonusshoot1)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire BonusTir1.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/BonusTir2.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_bonusshoot2)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire BonusTir2.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/gameover.ogg", FMOD_CREATESAMPLE, 0, &(res_data->sound_gameover)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire gameover.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/level1intro.ogg", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &(res_data->sound_levelintro)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire level1intro.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/level1music.ogg", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &(res_data->sound_level1)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire level1music.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/level2music.ogg", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &(res_data->sound_level2)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire level2music.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/level3music.ogg", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &(res_data->sound_level3)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire level3music.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/level4music.ogg", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &(res_data->sound_level4)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire level4music.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/level5music.ogg", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &res_data->sound_level5) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire level5music.ogg\n");
                exit(EXIT_FAILURE);
        }

        if (FMOD_System_CreateSound(system->sound, "./sounds/headshot.mp3", FMOD_CREATESAMPLE, 0, &(res_data->sound_headshot)) != FMOD_OK)
        {
                fprintf(stderr, "Impossible de lire headshot.mp3\n");
                exit(EXIT_FAILURE);
        }

}

void unload_res(t_res *res_data)
{

        /* ####################### */
        /* Dechargement des images */
        /* ####################### */

        SDL_FreeSurface(res_data->cursor);
        SDL_FreeSurface(res_data->player_face);
        SDL_FreeSurface(res_data->player_right);
        SDL_FreeSurface(res_data->player_left);
        SDL_FreeSurface(res_data->shot_skin);
        SDL_FreeSurface(res_data->bomb_skin);
        SDL_FreeSurface(res_data->plane_skin);
        SDL_FreeSurface(res_data->level1);
        SDL_FreeSurface(res_data->level2);
        SDL_FreeSurface(res_data->level3);
        SDL_FreeSurface(res_data->level4);
        SDL_FreeSurface(res_data->level5);


        /* ##################### */
        /* Dechargement des sons */
        /* ##################### */

        FMOD_Sound_Release(res_data->sound_playershoot);
        FMOD_Sound_Release(res_data->sound_playershoot2);
        FMOD_Sound_Release(res_data->sound_crash1);
        FMOD_Sound_Release(res_data->sound_crash2);
        FMOD_Sound_Release(res_data->sound_bonuslife);
        FMOD_Sound_Release(res_data->sound_bonusshoot1);
        FMOD_Sound_Release(res_data->sound_bonusshoot2);
        FMOD_Sound_Release(res_data->sound_gameover);
        FMOD_Sound_Release(res_data->sound_levelintro);
        FMOD_Sound_Release(res_data->sound_level1);
        FMOD_Sound_Release(res_data->sound_level2);
        FMOD_Sound_Release(res_data->sound_level3);
        FMOD_Sound_Release(res_data->sound_level4);
        FMOD_Sound_Release(res_data->sound_level5);
        FMOD_Sound_Release(res_data->sound_headshot);

}
