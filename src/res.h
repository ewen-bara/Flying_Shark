﻿/*
 * File:   res.h
 * Author: mstislav
 *
 * Created on 7 juin 2012, 09:35
 */

#ifndef RES_H
#define	RES_H

#include <SDL/SDL.h>
#include <fmodex/fmod.h>

#include "./system.h"

typedef struct
{

    SDL_Surface *cursor;
    SDL_Surface *player_face;
    SDL_Surface *player_right;
    SDL_Surface *player_left;
    SDL_Surface *shot_skin;
    SDL_Surface *bomb_skin;
    SDL_Surface *bomb_explo[12];
    SDL_Surface *plane_skin;
    SDL_Surface *level1;
    SDL_Surface *level2;
    SDL_Surface *level3;
    SDL_Surface *level4;
    SDL_Surface *level5;
    /*SDL_Surface *
    SDL_Surface *
    SDL_Surface * */

    FMOD_SOUND *sound_playershoot;
    FMOD_SOUND *sound_playershoot2;
    FMOD_SOUND *sound_crash1;
    FMOD_SOUND *sound_crash2;
    FMOD_SOUND *sound_bonuslife;
    FMOD_SOUND *sound_bonusshoot1;
    FMOD_SOUND *sound_bonusshoot2;
    FMOD_SOUND *sound_gameover;
    FMOD_SOUND *sound_levelintro;
    FMOD_SOUND *sound_level1;
    FMOD_SOUND *sound_level2;
    FMOD_SOUND *sound_level3;
    FMOD_SOUND *sound_level4;
    FMOD_SOUND *sound_level5;
    FMOD_SOUND *sound_headshot;

}t_res;

void load_res   (t_system *system, t_res *res_data);
void unload_res (t_res *res_data);

#endif	/* RES_H */

