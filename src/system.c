/*
system.c
------------

Par Durieux Alexis, pour le projet "Flying Shark"

Role : implementation des fonctions defini dans system.c
*/

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL_ttf.h>

#include "system.h"
#include "audio.h"


/* ####################################################################### */
/* Fonction quittant le programme et fermant l'objet systeme lie a l'audio */
/* ####################################################################### */

void exit_prog(t_system *system)
{
        TTF_Quit();
	clear_audio(system);
	exit(0);
}
