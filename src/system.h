/*
 * system.h
------------

Par Bara Ewen, pour le projet "Flying Shark"

Role : Definition type varaible system et definitions de la fonction pour quitter le jeu.
*/

#ifndef SYSTEM_H
#define	SYSTEM_H

#include <SDL/SDL.h>
#include <fmodex/fmod.h>

 typedef struct
 {
     SDL_Surface* screen;
     FMOD_SYSTEM*  sound;
 } t_system;

 void exit_prog(t_system *system);

#endif	/* SYSTEM_H */
